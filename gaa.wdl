workflow exercise {
    Array[File] read_datasets
    String gaa_container="bmannion/bundle:latest"
    Int quality = 30

    scatter (reads in read_datasets) {
        call fastqc {
            input: input_file=reads, container=gaa_container
        }
        call filter_reads {
            input: input_file=reads, quality=quality, container=gaa_container
        }
        call fastqc_filtered {
            input: input_file=filter_reads.pass_filtered_fq, quality=quality, container=gaa_container
        }
    }

    call short_assembly {
        input: input_file=filtered_reads[0], quality=quality, container=gaa_container
    }
    call qc_assembly as qc_short_assembly {
        input: contigs=assembly1_contigs, quality=quality, container=gaa_container
    }    

    call long_assembly {
        input: input_file=filtered_reads[1], quality=quality, container=gaa_container
    }
    call qc_assembly as qc_long_assembly {
        input: contigs=assembly2_contigs, quality=quality, container=gaa_container
    }

    output {
        Array[File] qc_files = fastqc.outhtml
        Array[File] filtered_reads = filter_reads.pass_filtered_fq
        Array[File] qc_filtered_files = fastqc_filtered.outhtml
        File assembly1_contigs = short_assembly.contigs
        File qc_assembly1 = qc_short_assembly.report
        File assembly2_contigs = long_assembly.contigs
        File qc_assembly2 = qc_long_assembly.report
    }
}

task fastqc {
    File input_file
    String base = basename(input_file, ".fastq")
    String container
    String outdir = "results/${base}/0_initialQC"

    runtime {
        docker: container
    }
    command {
        mkdir -p ${outdir}
        fastqc --outdir ${outdir} ${input_file} 
    }
    output {
        File outhtml = "${outdir}/${base}_fastqc.html"
    }
}

task filter_reads {
    File input_file
    String base = basename(input_file, ".fastq")
    Int quality
    String container
    
    String outdir = "results/${base}/1_filter"
    String unmatched = "${outdir}/${base}_qual${quality}_filtered.fq"
    String matched = "${outdir}/${base}_qual${quality}_matched.fq"
    String per_ref_stats = "${outdir}/${base}_qual${quality}_refstats.txt"

    Int trim_length = 100
    File illumina_adapters = "data/other/adapters.fa"
    File sequencing_artifacts = "data/other/sequencing_artifacts.fa.gz"
    File polyA = "data/other/polyA.fa.gz"
    File phiX_adapters = "data/other/phix_adapters.fa.gz"
    File phix174 = "data/other/phix174_ill.ref.fa.gz"
    File pacbio_adapters = "data/other/PacBioAdapter.fa"
    
    runtime {
        docker: container
    }
    command {
        mkdir -p ${outdir}
        bbduk.sh in=${input_file} out=${unmatched} outm=${matched} \
        qtrim=r trimq=${quality} \
        minlen=${trim_length} \
        ref=${illumina_adapters},${sequencing_artifacts},${polyA},${phiX_adapters},${phix174},${pacbio_adapters} \
        refstats=${per_ref_stats}
    }
    output {
        File pass_filtered_fq = unmatched
        File fail_filtered_fq = matched
        File refstats_outfile = per_ref_stats
    }
}

task fastqc_filtered {
    File input_file
    String base = basename(input_file, ".fq")
    Int quality
    String container

    String outdir = "results/${base}/2_postfilterQC"

    runtime {
        docker: container
    }
    command {
        mkdir -p ${outdir}
        fastqc --outdir ${outdir} ${input_file} 
    }
    output {
        File outhtml = "${outdir}/${base}_fastqc.html"
    }
}

task short_assembly {
    File input_file
    String base = basename(input_file, ".fq")
    Int quality
    String container

    String outdir = "results/${base}/3_assembly_qual${quality}"

    runtime {
        docker: container
    }
    command {
        spades.py -o ${outdir} --meta --12 ${input_file}
    }
    output {
        File contigs = "${outdir}/contigs.fasta"
    }
}

task long_assembly {
    File input_file
    String base = basename(input_file, ".fq")
    Int quality
    String container

    String outdir = "results/${base}/3_assembly_qual${quality}"

    runtime {
        docker: container
    }
    command {
        canu \
            -p ${base} -d ${outdir} \
            genomeSize=32m \
            minInputCoverage=0 \
            -pacbio-hifi ${input_file}
    }
    output {
        File contigs = "${outdir}/${base}.contigs.fasta"
    }
}

task qc_assembly {
    File contigs
    String base = basename(contigs, ".contigs.fasta")
    Int quality
    String container
    String outdir = "results/${base}/4_assemblyQC_qual${quality}"

    runtime {
        docker: container
    }
    command {
        quast.py --output-dir ${outdir} ${contigs}
    }
    output {
        File report = outdir + "/report.txt"
    }
}