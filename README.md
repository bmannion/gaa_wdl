# gaa_wdl

WDL workflow to process two sequence datasets from exercise.  

-----
## Setup/usage
### local
- Clone this repository
- With the two sequence FASTQs in `data/reads/`, run the workflow (`gaa.wdl`) via
```bash
# insert relevant cromwell version
$ java -jar path/to/cromwell-XX.jar --inputs inputs.json gaa.wdl
```
-----
### AWS
- Set up relevant EC2 instance
- Install `Docker` via amazon-linux-extras
- Pull Docker image `bmannion/bundle:latest`
- Install `cromwell` and `git` via conda
- Clone this repository
- With the two sequence FASTQs in `data/reads/`, run the workflow (`gaa.wdl`) via
```bash
$ cromwell run --inputs inputs.json gaa.wdl
```
- Be sure to terminate the EC2 instance after workflow completion
----- 

## Software (version)  
### Docker 
Image that bundles together several packages/software: `bmannion/bundle:latest`

- BBTools (39.01) https://jgi.doe.gov/data-and-tools/software-tools/bbtools/  
- canu (2.2) https://github.com/marbl/canu  
- FastQC (0.12.1) https://www.bioinformatics.babraham.ac.uk/projects/fastqc/  
- QUAST (5.2.0) https://quast.sourceforge.net/index.html  
- seqkit (2.4.0) https://bioinf.shenwei.me/seqkit/  
- SPAdes (3.15.4) https://cab.spbu.ru/software/spades/  

### Other
- cromwell (85) https://github.com/broadinstitute/cromwell/  
- WDL (1.0) https://openwdl.org/ 

-----

## Output
Output from this WDL workflow will be found within a separate `"../results/"` subdirectory within each executed task 

-----
## Issues/comments
- canu
  - Error/abort for task `long_assembly` due to low number of reads in `sequence_data2` dataset ... modified *OverlapErrorAdjustment.pm* based on response/suggestion in this issue: https://github.com/marbl/canu/issues/2035 ... this modified version of **canu** is in use (in the Docker image)
- Docker image is a bundle of several different packages/software (rather than separate and independent)

-----
## Contact
Brandon Mannion
